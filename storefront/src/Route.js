export const routes = {
    BASE_URL: "http://localhost:4000",
    PRODUCT: {
        get GET() {
            return `${routes.BASE_URL}/products`;
        }
    },
    ORDER: {
        get GET() {
            return `${routes.BASE_URL}/orders`;
        },
        get POST() {
            return `${routes.BASE_URL}/order`;
        }
    }
}