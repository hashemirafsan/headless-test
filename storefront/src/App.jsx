import React, {Component} from 'react';
import { Container, Row, Col, Card,
    CardBody, CardTitle, CardHeader,
    CardImg,
    CardSubtitle, ListGroup, ListGroupItem
} from 'reactstrap';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
    faCartPlus,
    faTimesCircle,
    faCheck,
    faTrashAlt
} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { toast, ToastContainer } from 'react-toastify';
import { routes } from './Route';
const axios = require('axios');


library.add(
    faCartPlus,
    faTimesCircle,
    faCheck,
    faTrashAlt
)


class App extends Component {

    constructor(prop) {
        super(prop);

        this.state = {
            products: [],
            cart_items: {},
            cart_total: 0,
            orders: []
        }

        this.toggleCartItem = this.toggleCartItem.bind(this);
        this.calculateTotal = this.calculateTotal.bind(this);
        this.removeCartItems = this.removeCartItems.bind(this);
        this.submitOrder = this.submitOrder.bind(this);
        this.callProducts = this.callProducts.bind(this);
        this.callOrders = this.callOrders.bind(this);
    }

    toggleCartItem = (index, product, action) => {
        let items = [...this.state.products];
        let cartItems = {...this.state.cart_items};
        let total = 0;

        if (!items[index].selected && !cartItems.hasOwnProperty(items[index].id) && action) {
            items[index].selected = true;
            cartItems[items[index].id] = Object.assign(items[index], {
                item: 1
            });
            total = this.state.cart_total + items[index].price;
            toast.success('🦄 Item added to cart!', {
                position: toast.POSITION.BOTTOM_LEFT
            });
        } else {
            total = this.state.cart_total - (cartItems[product.id].price * cartItems[product.id].item);
            delete cartItems[product.id];
            items = items.map((item) => {
                if (item.id === product.id) {
                    item.selected = false;
                }
                return item;
            })
            toast.warning('🦄Item removed from cart!', {
                position: toast.POSITION.BOTTOM_LEFT
            });
        }
        if (! window.localStorage.getItem('headless_cart_items')) {
            let data = {
                cart_items: cartItems,
                cart_total: total
            };
            window.localStorage.setItem('headless_cart', JSON.stringify(data));
        }

        this.setState({ products: items, cart_items: cartItems, cart_total: total});
    }

    updateCartItem = (index, evt) => {
        let cartItems = {...this.state.cart_items};
        cartItems[index].item = evt.target.value >= 0 ? evt.target.value : 0;
        this.setState({cart_items: cartItems});
        let cart = JSON.parse(window.localStorage.getItem('headless_cart'));
        cart.cart_items = cartItems;
        window.localStorage.setItem('headless_cart', JSON.stringify(cart));
        this.calculateTotal();
    }

    calculateTotal = () => {
        let cartItems = {...this.state.cart_items};
        let total = 0;
        Object.keys(cartItems).map((index) => total += (cartItems[index].item * cartItems[index].price))
        this.setState({ cart_total: total })
        let cart = JSON.parse(window.localStorage.getItem('headless_cart'));
        cart.cart_total = total;
        window.localStorage.setItem('headless_cart', JSON.stringify(cart));
    }

    removeCartItems = (alert = true) => {
        this.setState({
            products: this.state.products.map((product) => {
                product.selected = false; return product;
            }),
            cart_items: {},
            cart_total: 0
        })
        window.localStorage.setItem('headless_cart', JSON.stringify({cart_items: {}, cart_total: 0}));
        if (alert) {
            toast.warning("Cart items removed", {
                position: toast.POSITION.BOTTOM_RIGHT
            })
        }
    }

    submitOrder = () => {
        axios.post(routes.ORDER.POST, {
            cart_items: this.state.cart_items,
            cart_total: this.state.cart_total
        })
            .then(({ data: { message }}) => {
                this.removeCartItems(false);
                toast.success(message, {
                    position: toast.POSITION.BOTTOM_RIGHT
                })
                this.callOrders();
            })
            .catch((err) => {

            })
    }

    callProducts = () => {
        console.log(routes);
        fetch(routes.PRODUCT.GET).then(res => {
            return res.json();
        }).then(data => {
            let cart = null;
            let cartItems = {};
            let cartTotal = 0;
            if (( cart = JSON.parse(window.localStorage.getItem('headless_cart')))) {
                cartItems = cart.cart_items;
                cartTotal = cart.cart_total;
            }
            data = data.map(function (item) {
                return Object.assign(item, {
                    selected: cartItems.hasOwnProperty(item.id)
                });
            })
            this.setState({products: data, cart_items: cartItems, cart_total: cartTotal})
        });
    }

    callOrders = () => {
        fetch(routes.ORDER.GET).then(res => {
            return res.json();
        }).then(data => {
            this.setState({ orders: data })
        });
    }

    componentDidMount = () => {
        this.callProducts();
        this.callOrders();
    }

    render() {
        return (
            <div className="App">
                <header className="App-header"></header>
                <main className="mt-5">
                    <Container>
                        <Row>
                            <Col xs="8">
                                <h2>Your Product</h2>
                                <Row>
                                    {
                                        this.state.products.map((product, key) => {
                                            return (
                                                <Col xs="4" key={key} className="mb-3">
                                                    <Card>
                                                        <CardImg top width="200px" height="150px" src="/assets/placeholder.webp" alt="Card image cap" />
                                                        <CardBody>
                                                            <CardTitle>{product.name}</CardTitle>
                                                            <CardSubtitle>Good Product</CardSubtitle>
                                                            <div>
                                                                <span className="pull-left">${product.price}</span>
                                                                <button className="btn btn-circle" onClick={() => product.selected ? this.toggleCartItem(key, product, false) : this.toggleCartItem(key, product, true)}>
                                                                    <FontAwesomeIcon icon={product.selected ? faCheck :faCartPlus} />
                                                                </button>
                                                            </div>
                                                        </CardBody>
                                                    </Card>
                                                </Col>
                                            )
                                        })
                                    }
                                </Row>
                            </Col>
                            <Col xs="4">
                                <Card>
                                    <CardHeader className="flex-container">
                                        <div>
                                            <h4>Your Cart</h4>
                                        </div>
                                        <div className="pull-right">
                                            <button className="btn btn-default" onClick={this.removeCartItems}><FontAwesomeIcon icon={faTrashAlt} /> Clear</button>
                                        </div>
                                    </CardHeader>
                                    <CardBody >
                                        <ListGroup className="full-height">
                                            {
                                                Object.keys(this.state.cart_items).map((index, key) => {
                                                    return (
                                                        <ListGroupItem key={key}>
                                                            <div className="flex-container">
                                                                <div>
                                                                    <span>{ this.state.cart_items[index].name }</span>
                                                                </div>
                                                                <div>
                                                                    <input type="number" className="qty" min="0" value={ this.state.cart_items[index].item }
                                                                           onChange={(evt) => this.updateCartItem(index, evt)}
                                                                    />
                                                                </div>
                                                                <div>
                                                                    ${ this.state.cart_items[index].price }
                                                                </div>
                                                                <div>
                                                                    <button className="btn btn-xs btn-danger" onClick={() => this.toggleCartItem(index, this.state.cart_items[index], false)}>
                                                                        <FontAwesomeIcon icon={faTimesCircle}/>
                                                                    </button>
                                                                </div>
                                                            </div>
                                                        </ListGroupItem>
                                                    );
                                                })
                                            }
                                        </ListGroup>
                                        <div className="place-order">
                                            <div className="flex-container">
                                                <div>
                                                    <button className="btn brand-color" onClick={this.submitOrder}>Place Order</button>
                                                </div>
                                                <div>
                                                    <strong><span>Grand Total ${ this.state.cart_total }</span></strong>
                                                </div>
                                            </div>
                                        </div>
                                    </CardBody>
                                </Card>
                            </Col>
                        </Row>
                        <Row>
                            <Col xs="12">
                                <h2>Your orders</h2>
                                <Row>
                                    {
                                        this.state.orders.map((orders, key) => {
                                            return (
                                                <Col xs="4" key={key} className="mb-3">
                                                    <Card>
                                                        <CardHeader># Order {key+1}</CardHeader>
                                                        <CardBody>
                                                            <ListGroup>
                                                                <ListGroupItem key={key}>
                                                                    <div className="flex-container">
                                                                        <div>
                                                                            <span>Name</span>
                                                                        </div>
                                                                        <div>
                                                                            <span>Qty</span>
                                                                        </div>
                                                                        <div>
                                                                            <span>Price</span>
                                                                        </div>
                                                                    </div>
                                                                </ListGroupItem>
                                                            </ListGroup>
                                                            <ListGroup>
                                                                {
                                                                    Object.keys(orders.cart_items).map((order, key) => {
                                                                        return (
                                                                            <ListGroupItem key={key}>
                                                                                <div className="flex-container">
                                                                                    <div>
                                                                                        <span>{ orders.cart_items[order].name }</span>
                                                                                    </div>
                                                                                    <div>
                                                                                        <span>{ orders.cart_items[order].item}</span>
                                                                                    </div>
                                                                                    <div>
                                                                                        <span>${ orders.cart_items[order].price }</span>
                                                                                    </div>
                                                                                </div>
                                                                            </ListGroupItem>
                                                                        )
                                                                    })
                                                                }
                                                            </ListGroup>
                                                        </CardBody>
                                                    </Card>
                                                </Col>
                                            )
                                        })
                                    }
                                </Row>
                            </Col>
                        </Row>
                    </Container>
                </main>
                <ToastContainer />
            </div>
        );
    }
}

export default App;
