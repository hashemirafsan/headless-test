const express = require('express');
const cors = require('cors');
const fs = require('fs');
const path = require('path');
const app = express();

app.use(cors());
app.use(express.urlencoded());
app.use(express.json());

const PORT = 4000;

app.get('/products', (req, res) => {
   fs.readFile(path.join(__dirname + '/data/products.json'), 'utf8', (err, data) => {
      if (err) {console.log(err); return;}
      return res.json(JSON.parse(data));
   });
});

app.get('/orders', (req, res) => {
   fs.readFile(path.join(__dirname + '/data/orders.json'), 'utf8', (err, data) => {
      if (err) {console.log(err); return;}
      return res.json(JSON.parse(data));
   });
})

app.post('/order', (req, res) => {
   let order = {
      cart_items: req.body.cart_items,
      cart_total: req.body.cart_total
   }
   fs.readFile(path.join(__dirname + '/data/orders.json'), 'utf8', (err, data) => {
      console.log(err, data);
      if (err) {console.log(err); return;}
      let orders = JSON.parse(data);
      orders.push(order)
      fs.writeFile(path.join(__dirname + '/data/orders.json'), JSON.stringify(orders), 'utf8', (err, data) => {});
   });

   return res.json({
      message: "successfully cart added"
   });
});

app.listen(PORT, () => console.log(`App is listening on port ${PORT}...`));